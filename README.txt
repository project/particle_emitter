particle_emitter module.
==============================================================================
Particle Emitter (C) 2013 by A.Wagner (http://aw030.de)
==============================================================================

Particle Emitter is a highly configurable module which provides a particle emitter system using HTML5 canvas.

Installation
------------------------------------------------------------------------------

The installation is very simple:

  I. Installation
  II. Configuration

I. Installation

  1) Moving to module folder:
     Move the downloaded module package to sites/all/modules.
     => sites/all/modules/particle_emitter/
     
  2) Activating the module:
     In administration back-end on the module configuration (admin/modules) 
     page you have now to install/activate the module and save the module 
     configuration.
  
  3) Activating and placing the block:
     After activation of the module e new block is added to the block 
     configuration page under structure/blocks. Now move the new block to 
     the desired region and make some block specific configuration.

II. Configuration

  1) After installation you should check the configuration page for the 
     particle_emitter module and customize the settings. Options are self 
     explaining in the administration form.

------------------------------------------------------------------------------
Module written by A.Wagner on http://drupal.org
------------------------------------------------------------------------------
