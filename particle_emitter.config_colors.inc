<?php
/**
 * @file
 * particle_emitter.config_colors.inc.
 */

 /**
  * Implements hook_form().
  */
function particle_emitter_colors_form($form, &$form_state) {

  $form['particle_emitter_group_colors'] = array(
    '#type' => 'fieldset',
    '#title' => t('Colors'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['particle_emitter_group_colors']['particle_emitter_group_colors_canvas'] = array(
    '#type' => 'fieldset',
    '#title' => t('Canvas Coloration'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['particle_emitter_group_colors']['particle_emitter_group_colors_canvas']['particle_emitter_canvas_fill_style'] = array(
    '#type' => 'select',
    '#title' => t('Fill Style'),
    '#options' => array(
      'full' => t('Full')),
    '#default_value' => variable_get('particle_emitter_canvas_fill_style', 'gradient'),
    '#description' => t('Select the filling type for the canvas.'),
  );

  $form['particle_emitter_group_colors']['particle_emitter_group_colors_canvas']['particle_emitter_canvas_bgcolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Canvas bg color'),
    '#description' => t('Canvas background color.'),
    '#default_value' => variable_get('particle_emitter_canvas_bgcolor', '#FFFFFF'),
    '#size' => 40,
    '#maxlength' => 120,
    '#required' => FALSE,
  );

  $form['particle_emitter_group_colors']['particle_emitter_group_colors_particles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Particles Coloration'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['particle_emitter_group_colors']['particle_emitter_group_colors_particles']['particle_emitter_particles_fill_style'] = array(
    '#type' => 'select',
    '#title' => t('Fill Style'),
    '#options' => array(
      'gradient' => t('Gradient'),
      'full' => t('Full')),
    '#default_value' => variable_get('particle_emitter_particles_fill_style', 'gradient'),
    '#description' => t('Select the filling style for the particles.'),
  );

  $form['particle_emitter_group_colors']['particle_emitter_group_colors_particles']['particle_emitter_particles_fill_type'] = array(
    '#type' => 'select',
    '#title' => t('Fill Type'),
    '#options' => array(
      'random' => t('Random'),
      'fixed' => t('Fixed')),
    '#default_value' => variable_get('particle_emitter_particles_fill_type', 'gradient'),
    '#description' => t('Select the filling type for the particles. Colors will be applied randomly or by the defined rgb color below.'),
  );

  $form['particle_emitter_group_colors']['particle_emitter_group_colors_particles']['particle_emitter_particles_color_rgb_r'] = array(
    '#type' => 'textfield',
    '#title' => t('RGB - Red'),
    '#description' => t('RGB - red component.'),
    '#default_value' => variable_get('particle_emitter_particles_color_rgb_r', 0),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => FALSE,
  );

  $form['particle_emitter_group_colors']['particle_emitter_group_colors_particles']['particle_emitter_particles_color_rgb_g'] = array(
    '#type' => 'textfield',
    '#title' => t('RGB - Green'),
    '#description' => t('RGB - green component.'),
    '#default_value' => variable_get('particle_emitter_particles_color_rgb_g', 0),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => FALSE,
  );

  $form['particle_emitter_group_colors']['particle_emitter_group_colors_particles']['particle_emitter_particles_color_rgb_b'] = array(
    '#type' => 'textfield',
    '#title' => t('RGB - Blue'),
    '#description' => t('RGB - blue component.'),
    '#default_value' => variable_get('particle_emitter_particles_color_rgb_b', 0),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );

  return $form;
}

 /**
  * Implements hook_form_submit().
  */
function particle_emitter_colors_form_submit($form, &$form_state) {

  variable_set('particle_emitter_canvas_fill_style', $form_state['values']['particle_emitter_canvas_fill_style']);
  variable_set('particle_emitter_canvas_bgcolor', $form_state['values']['particle_emitter_canvas_bgcolor']);
  variable_set('particle_emitter_particles_fill_style', $form_state['values']['particle_emitter_particles_fill_style']);
  variable_set('particle_emitter_particles_fill_type', $form_state['values']['particle_emitter_particles_fill_type']);
  variable_set('particle_emitter_particles_color_rgb_r', $form_state['values']['particle_emitter_particles_color_rgb_r']);
  variable_set('particle_emitter_particles_color_rgb_g', $form_state['values']['particle_emitter_particles_color_rgb_g']);
  variable_set('particle_emitter_particles_color_rgb_b', $form_state['values']['particle_emitter_particles_color_rgb_b']);

  drupal_set_message(t('The settings have been saved'));
}
