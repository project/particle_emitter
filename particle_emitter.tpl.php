<?php

/**
 * @file
 * HTML for particle_emitter.
 *
 * @see template_preprocess_particle_emitter()
 */
?>
<div id="particle-emitter" class="<?php print $classes; ?>"></div>
