<?php
/**
 * @file
 * particle_emitter.config_emitter.inc.
 */

 /**
  * Implements hook_form().
  */
function particle_emitter_emitter_form($form, &$form_state) {

  $form['particle_emitter_group_emitter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Emitter'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['particle_emitter_group_emitter']['particle_emitter_emitter_position_type'] = array(
    '#type' => 'select',
    '#title' => t('Emitter location in canvas'),
    '#options' => array(
      'center' => t('Center'),
      'mouse' => t('Mouse'),
      'offset' => t('Offset')),
    '#default_value' => variable_get('particle_emitter_emitter_position_type', 'center'),
    '#description' => t('Select emitter location in canvas.'),
  );

  $form['particle_emitter_group_emitter']['particle_emitter_emitter_offset_x'] = array(
    '#type' => 'textfield',
    '#title' => t('Emitter X-offset'),
    '#description' => t('Define the x-offset.'),
    '#default_value' => variable_get('particle_emitter_emitter_offset_x', 0),
    '#size' => 6,
    '#maxlength' => 6,
    '#required' => FALSE,
  );

  $form['particle_emitter_group_emitter']['particle_emitter_emitter_offset_y'] = array(
    '#type' => 'textfield',
    '#title' => t('Emitter Y-offset'),
    '#description' => t('Define the y-offset.'),
    '#default_value' => variable_get('particle_emitter_emitter_offset_y', 0),
    '#size' => 6,
    '#maxlength' => 6,
    '#required' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );

  return $form;
}

 /**
  * Implements hook_form_submit().
  */
function particle_emitter_emitter_form_submit($form, &$form_state) {

  variable_set('particle_emitter_emitter_position_type', $form_state['values']['particle_emitter_emitter_position_type']);
  variable_set('particle_emitter_emitter_offset_x', $form_state['values']['particle_emitter_emitter_offset_x']);
  variable_set('particle_emitter_emitter_offset_y', $form_state['values']['particle_emitter_emitter_offset_y']);

  drupal_set_message(t('The settings have been saved'));
}
