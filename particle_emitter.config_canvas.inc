<?php
/**
 * @file
 * particle_emitter.config_canvas.inc.
 */

 /**
  * Implements hook_form().
  */
function particle_emitter_canvas_form($form, &$form_state) {

  $form['particle_emitter_group_canvas'] = array(
    '#type' => 'fieldset',
    '#title' => t('Canvas'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['particle_emitter_group_canvas']['particle_emitter_canvas_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Canvas width'),
    '#description' => t('Canvas width (px).'),
    '#default_value' => variable_get('particle_emitter_canvas_width', 0),
    '#size' => 5,
    '#maxlength' => 5,
    '#required' => FALSE,
  );

  $form['particle_emitter_group_canvas']['particle_emitter_canvas_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Canvas height'),
    '#description' => t('Canvas height (px).'),
    '#default_value' => variable_get('particle_emitter_canvas_height', 0),
    '#size' => 5,
    '#maxlength' => 5,
    '#required' => FALSE,
  );

  $form['particle_emitter_group_canvas']['particle_emitter_canvas_composite_type'] = array(
    '#type' => 'select',
    '#title' => t('Canvas FX'),
    '#options' => _particle_emitter_get_composite_types(),
    '#default_value' => variable_get('particle_emitter_canvas_composite_type', 'source-over'),
    '#description' => t('Specify effect of canvas depending on composite type per frame.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );

  return $form;
}

 /**
  * Implements hook_form_submit().
  */
function particle_emitter_canvas_form_submit($form, &$form_state) {

  variable_set('particle_emitter_canvas_width', $form_state['values']['particle_emitter_canvas_width']);
  variable_set('particle_emitter_canvas_height', $form_state['values']['particle_emitter_canvas_height']);
  variable_set('particle_emitter_canvas_composite_type', $form_state['values']['particle_emitter_canvas_composite_type']);

  drupal_set_message(t('The settings have been saved'));
}
