<?php
/**
 * @file
 * particle_emitter.config_particles.inc.
 */

 /**
  * Implements hook_form().
  */
function particle_emitter_particles_form($form, &$form_state) {

  $form['particle_emitter_group_particles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Particles'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['particle_emitter_group_particles']['particle_emitter_particle_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Particle count'),
    '#description' => t('Define the amount of particles.'),
    '#default_value' => variable_get('particle_emitter_particle_count', 10),
    '#size' => 4,
    '#maxlength' => 4,
    '#required' => FALSE,
  );

  $form['particle_emitter_group_particles']['particle_emitter_particle_composite_type'] = array(
    '#type' => 'select',
    '#title' => t('Particle FX'),
    '#options' => _particle_emitter_get_composite_types(),
    '#default_value' => variable_get('particle_emitter_particle_composite_type', 'darker'),
    '#description' => t('Specify effect of particles depending on composite type per frame.'),
  );

  $form['particle_emitter_group_particles']['particle_emitter_group_particles_life'] = array(
    '#type' => 'fieldset',
    '#title' => t('Particle life'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['particle_emitter_group_particles']['particle_emitter_group_particles_life']['particle_emitter_particle_life'] = array(
    '#type' => 'select',
    '#title' => t('Life type'),
    '#options' => array(
      'random' => t('Scope'),
      'fixed' => t('Fixed')),
    '#default_value' => variable_get('particle_emitter_particle_life', 'random'),
    '#description' => t('Select the life mode. "Fixed" lifetime will use "Lifetime fixed", "Scope" will use a random value between "Lifetime min" and "Lifetime max".'),
  );

  $form['particle_emitter_group_particles']['particle_emitter_group_particles_life']['particle_emitter_particle_life_fix'] = array(
    '#type' => 'textfield',
    '#title' => t('Lifetime fixed'),
    '#description' => t('Define fixed lifetime.'),
    '#default_value' => variable_get('particle_emitter_particle_life_fix', 10),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => FALSE,
  );

  $form['particle_emitter_group_particles']['particle_emitter_group_particles_life']['particle_emitter_particle_life_min'] = array(
    '#type' => 'textfield',
    '#title' => t('Lifetime min'),
    '#description' => t('Define minimum lifetime.'),
    '#default_value' => variable_get('particle_emitter_particle_life_min', 10),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => FALSE,
  );

  $form['particle_emitter_group_particles']['particle_emitter_group_particles_life']['particle_emitter_particle_life_max'] = array(
    '#type' => 'textfield',
    '#title' => t('Lifetime max'),
    '#description' => t('Define maximum lifetime.'),
    '#default_value' => variable_get('particle_emitter_particle_life_max', 20),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => FALSE,
  );

  $form['particle_emitter_group_particles']['particle_emitter_group_particles_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Particle Types'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['particle_emitter_group_particles']['particle_emitter_group_particles_types']['particle_emitter_particle_type'] = array(
    '#type' => 'select',
    '#title' => t('Particle type'),
    '#options' => array(
      'circle' => t('Circle')),
    '#default_value' => variable_get('particle_emitter_particle_type', 'circle'),
    '#description' => t('Select the particle type.'),
  );

  $form['particle_emitter_group_particles']['particle_emitter_group_particles_types']['particle_emitter_group_particles_circle'] = array(
    '#type' => 'fieldset',
    '#title' => t('Circles'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['particle_emitter_group_particles']['particle_emitter_group_particles_types']['particle_emitter_group_particles_circle']['particle_emitter_particle_radius'] = array(
    '#type' => 'select',
    '#title' => t('Radius'),
    '#options' => array(
      'random' => t('Scope'),
      'fixed' => t('Fixed')),
    '#default_value' => variable_get('particle_emitter_particle_radius', 'random'),
    '#description' => t('Select the radius mode. "Fixed" radius will use "Radius fixed", "Scope" will use a random value between "Radius min" and "Radius max".'),
  );

  $form['particle_emitter_group_particles']['particle_emitter_group_particles_types']['particle_emitter_group_particles_circle']['particle_emitter_particle_radius_transform'] = array(
    '#type' => 'select',
    '#title' => t('Radius'),
    '#options' => array(
      'off' => t('Off'),
      'shrink' => t('Shrink'),
      'grow' => t('Grow')),
    '#default_value' => variable_get('particle_emitter_particle_radius_transform', 'shrink'),
    '#description' => t('Select the radius transformation mode. Shrinking will affect the lifetime and particle will die if radius becomes 0.'),
  );

  $form['particle_emitter_group_particles']['particle_emitter_group_particles_types']['particle_emitter_group_particles_circle']['particle_emitter_particle_radius_fix'] = array(
    '#type' => 'textfield',
    '#title' => t('Radius fixed'),
    '#description' => t('Define fixed radius.'),
    '#default_value' => variable_get('particle_emitter_particle_radius_fix', 10),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => FALSE,
  );

  $form['particle_emitter_group_particles']['particle_emitter_group_particles_types']['particle_emitter_group_particles_circle']['particle_emitter_particle_radius_min'] = array(
    '#type' => 'textfield',
    '#title' => t('Radius min'),
    '#description' => t('Define minimum radius.'),
    '#default_value' => variable_get('particle_emitter_particle_radius_min', 10),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => FALSE,
  );

  $form['particle_emitter_group_particles']['particle_emitter_group_particles_types']['particle_emitter_group_particles_circle']['particle_emitter_particle_radius_max'] = array(
    '#type' => 'textfield',
    '#title' => t('Radius max'),
    '#description' => t('Define maximum radius.'),
    '#default_value' => variable_get('particle_emitter_particle_radius_max', 30),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );

  return $form;
}

 /**
  * Implements hook_form_submit().
  */
function particle_emitter_particles_form_submit($form, &$form_state) {

  variable_set('particle_emitter_particle_count', $form_state['values']['particle_emitter_particle_count']);
  variable_set('particle_emitter_particle_composite_type', $form_state['values']['particle_emitter_particle_composite_type']);
  variable_set('particle_emitter_particle_life', $form_state['values']['particle_emitter_particle_life']);
  variable_set('particle_emitter_particle_life_fix', $form_state['values']['particle_emitter_particle_life_fix']);
  variable_set('particle_emitter_particle_life_min', $form_state['values']['particle_emitter_particle_life_min']);
  variable_set('particle_emitter_particle_life_max', $form_state['values']['particle_emitter_particle_life_max']);
  variable_set('particle_emitter_particle_type', $form_state['values']['particle_emitter_particle_type']);
  variable_set('particle_emitter_particle_radius', $form_state['values']['particle_emitter_particle_radius']);
  variable_set('particle_emitter_particle_radius_transform', $form_state['values']['particle_emitter_particle_radius_transform']);
  variable_set('particle_emitter_particle_radius_fix', $form_state['values']['particle_emitter_particle_radius_fix']);
  variable_set('particle_emitter_particle_radius_min', $form_state['values']['particle_emitter_particle_radius_min']);
  variable_set('particle_emitter_particle_radius_max', $form_state['values']['particle_emitter_particle_radius_max']);

  drupal_set_message(t('The settings have been saved'));
}
